<?php

namespace Drupal\comment_alert\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class for implementing module configuration form.
 */
class CommentAlertConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'comment_alert_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('comment_alert.settings');

    $form['alerts'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Choose who will receive a comment alert email'),
    );

    $form['alerts']['alert_node_author'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Report to node author (reply node)'),
      '#default_value' => $config->get('alert_node_author'),
    );

    $form['alerts']['alert_comment_author'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Report to comment author (reply comment)'),
      '#default_value' => $config->get('alert_comment_author'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('comment_alert.settings');
    $config->set('alert_node_author', $form_state->getValue('alert_node_author'));
    $config->set('alert_comment_author', $form_state->getValue('alert_comment_author'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'comment_alert.settings',
    ];
  }

}
