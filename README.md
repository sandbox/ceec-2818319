Comment alert
=============
Lorem ipsum generator for Drupal.


About
-----
Developed by Álvaro Galán @ www.solucionex.com


Uses
-----
The Comment Alert module allow your site send an email when a new comment is posted. You can:

* Report to the author of the node where the comment has been created.
* Report to the author of a comment if has been created as comment reply.
